# KIOSK LAMBDA

This lambda provides us sending alarm notification to the Slack and MS Teams.

## INSTALATION

 - Clone repo into your comp
 - Install necessary package:

        npm install

 - Zip all content:

        zip -r kiosk-lambda.zip *

 - Upload to S3 bucket or direct to the lambda


## VARIABLE
| Variable | Description |
| :--- | :--- |
| WEBHOOK_URL | Slack webhook URL |
| US_GI_WEBHOOK | US webhook for general issues |
| US_WARN_WEBHOOK | US webhook for hardware warning issues |
| US_HI_WEBHOOK | US webhook for hardware alarms issues |
| US_NET_WEBHOOK | US webhook for network connection issues |
| GB_GI_WEBHOOK | UK webhook for general issues |
| GB_WARN_WEBHOOK | UK webhook for hardware warning issues |
| GB_HI_WEBHOOK | UK webhook for hardware alarms issues |
| GB_NET_WEBHOOK | UK webhook for network connection issues|
| ES_GI_WEBHOOK | ES webhook for general issues |
| ES_WARN_WEBHOOK | ES webhook for hardware warning issues |
| ES_HI_WEBHOOK | ES webhook for hardware alarms issues |
| ES_NET_WEBHOOK | ES webhook for network connection issues |
