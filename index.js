const request = require('request-promise-native');

// SEND MSG TO TEAMS

const sendMessage = (msg, webhoook) => {
  return request({
    method: 'POST',
    url: webhoook,
    body: msg,
    json: true,
  })
    .then((body) => {
      if (body === 'ok') {
        return {};
      } else {
        throw new Error(body);
      }
    });

};

function getDimensionData(dim,variable) {
  function getData(data) {
    return data.name === variable;
  }
  var dimensions = dim.find(getData);

  return dimensions.value;
}

function teamsWebhook(code, metric){
  if (code === "US"){
    if (metric === 'GeneralIssue'){
      return process.env.US_GI_WEBHOOK;
    } else if (metric === 'Warning'){
      return process.env.US_WARN_WEBHOOK;
    } else if (metric === 'HardwareIssue'){
      return process.env.US_HI_WEBHOOK;
    } else if (metric === 'TemporaryInternetIssue'){
      return process.env.US_NET_WEBHOOK;
    } else {
      return;
    }
  } else if (code === "GB") {
    if (metric === 'GeneralIssue'){
      return process.env.GB_GI_WEBHOOK;
    } else if (metric === 'Warning'){
      return process.env.GB_WARN_WEBHOOK;
    } else if (metric === 'HardwareIssue'){
      return process.env.GB_HI_WEBHOOK;
    } else if (metric === 'TemporaryInternetIssue'){
      return process.env.GB_NET_WEBHOOK;
    } else {
      return;
    }
  } else if (code === "ES"){
    if (metric === 'GeneralIssue'){
      return process.env.ES_GI_WEBHOOK;
    } else if (metric === 'Warning'){
      return process.env.ES_WARN_WEBHOOK;
    } else if (metric === 'HardwareIssue'){
      return process.env.ES_HI_WEBHOOK;
    } else if (metric === 'TemporaryInternetIssue'){
      return process.env.ES_NET_WEBHOOK;
    } else {
      return;
    }
  } else {
    return;
  }
}

// ALARM STATUS COLOR
function alarmStatus(status){
  var color_status;

  if (status === 'ALARM') {
    return color_status = "#DF5E42";
  }
  else if (status === 'OK') {
   return color_status = "#36a64f";
  }
  else {
    return color_status = "#C5C5C5";
  }
}

// PARSE MSG SLACK
function parseMessageSlack(subject, msgArray, color){
  var dimArray = msgArray.Trigger.Dimensions;
  var kiosk = getDimensionData(dimArray, "Kiosk ID");
  var country = getDimensionData(dimArray, "Country code");
  var terminal = getDimensionData(dimArray, "Terminal ID");
  var location = getDimensionData(dimArray, "Location name");

  var result = [];
  for (var i = 0; i < dimArray.length; i++) {
  	result.push({ title: dimArray[i].name, value: dimArray[i].value, short: true });
  }
  result.push({ title: "Time", value: msgArray.StateChangeTime, short: true });
  result.push({ title: "Metric", value: msgArray.Trigger.MetricName , short: true });
  result.push({ title: "Terminal Page", value: "https://www.parkjockey.com/en-us/admin/all/admin_terminals/view/"+terminal });

  var message = {
    "text": subject,
    "attachments": [{
      "color": color,
      "title": "Dashboard",
      "title_link": "https://monitoring.parkjockey.com/d/9xNOwC9Wz/template-dashboard?orgId=1&var-country="+ country +"&var-location="+location+"&var-kioskID="+kiosk+"&var-terminalID="+ terminal,
      "text": "More info about alarm on link",
      "fields": result,
      "footer": "AWS Cloudwatch",
      "footer_icon": "https://cdn2.iconfinder.com/data/icons/amazon-aws-stencils/100/Deployment__Management_copy_CloudWatch-512.png"
    }]
  };

  return message;
}

// PARSE MSG TEAMS
function parseMessageTeams(subject,msg,color){
  var all_dimensions = msg.Trigger.Dimensions;
  all_dimensions.push({ name: "Metric Name", value: msg.Trigger.MetricName});
  all_dimensions.push({ name: "Time", value: msg.StateChangeTime});
  var kiosk = getDimensionData(all_dimensions, "Kiosk ID");
  var country = getDimensionData(all_dimensions, "Country code");
  var terminal = getDimensionData(all_dimensions, "Terminal ID");
  var location = getDimensionData(all_dimensions, "Location name");

  var message = {
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": color,
    "summary":"AWS Cloudwatch",
    "sections":[
      {
         "activityTitle": subject,
         "facts": all_dimensions
      }],
      "potentialAction":[
         {
            "@context":"http://schema.org",
            "@type":"ViewAction",
            "name":"View in Grafana",
            "target":[
               "https://monitoring.parkjockey.com/d/9xNOwC9Wz/template-dashboard?orgId=1&var-country="+ country +"&var-location="+location+"&var-kioskID="+kiosk+"&var-terminalID="+ terminal
            ]
         },
         {
            "@context":"http://schema.org",
            "@type":"ViewAction",
            "name":"View Terminal Page",
            "target": [
                "https://www.parkjockey.com/en-us/admin/all/admin_terminals/view/" + terminal
            ]           
         }
      ]
  };

  return message;
}


const sendSlack = (record) => {
  const subject = record.Sns.Subject;
  const message = JSON.parse(record.Sns.Message);
  var color = alarmStatus(message.NewStateValue);
  var msg = parseMessageSlack(subject, message,color);
  var webhook = process.env.WEBHOOK_URL;
  
  if (message.OldStateValue === "INSUFFICIENT_DATA" && message.NewStateValue === "OK") {
    return;
  }
  else {
    return sendMessage(msg,webhook);
  }

};

const sendTeams = (record) => {
  const subject = record.Sns.Subject;
  const message = JSON.parse(record.Sns.Message);
  var color = alarmStatus(message.NewStateValue);
  var teamsMsg = parseMessageTeams(subject,message,color);
  var webhook = teamsWebhook(getDimensionData(message.Trigger.Dimensions, "Country code"),message.Trigger.MetricName);

  if (message.OldStateValue === "INSUFFICIENT_DATA" && message.NewStateValue === "OK") {
    return;
  }
  else {
    return sendMessage(teamsMsg,webhook);
  }

};

exports.handler = (event, context, cb) => {
  console.log(`event received: ${JSON.stringify(event)}`);
  Promise.all(event.Records.map(sendSlack))
    .then(() => cb(null))
    .catch((err) => cb(err));
  Promise.all(event.Records.map(sendTeams))
    .then(() => cb(null))
    .catch((err) => cb(err));
};